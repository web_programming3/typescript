interface Rectangle {
    width: number;
    hight : number;
}
interface ColorRectangle extends Rectangle {
    color: string;

}

const rectangle : Rectangle = {
    width: 20,
    hight: 10
}
console.log(rectangle);

const colorRectangle : ColorRectangle = {
    width: 20,
    hight: 10,
    color: "red"
}

console.log(colorRectangle);
