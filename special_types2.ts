let w : unknown = 1;
w = "String";
w = {
    runAnnonExistentMethod:() =>{
        console.log("I think therefore I am");
    }
}as { runAnnonExistentMethod: () => void}
if(typeof w === 'object' && w!== null){
    (w as { runAnnonExistentMethod: Function }).runAnnonExistentMethod();
}
