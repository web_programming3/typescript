type CarYear = number;
type CarType = String;
type CarModel = String;

type Car = {
    year : CarYear,
    type: CarType,
    model : CarModel,
}

const CarYear : CarYear = 2001;
const CarType : CarType = "BMW";
const CarModel : CarModel = "Z4"

const car1 : Car = {
    year : CarYear,
    type : CarType,
    model : CarModel,
}
console.log(car1);